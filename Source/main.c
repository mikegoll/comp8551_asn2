#include "../Headers/main.h"
#include "../Headers/openCLWrappers.h"

#include <stdio.h>

int main() {
    //CL_DEVICE_TYPE_CPU
    //CL_DEVICE_TYPE_GPU
    setupOpenCL(CL_DEVICE_TYPE_ALL);
    return 0;
}

void setupOpenCL(cl_mem_flags flagsType) {
    //OpenCL data
   cl_device_id device;
   cl_context context;
   cl_program program;
   cl_kernel kernel;
   cl_command_queue queue;
   cl_mem memObjects[3] = {0, 0, 0};
   cl_int i, j, err;
   size_t local_size, global_size;
   cl_int numGroups;

   float sum[2];

   //Get devices and context
   if ((context = createContext(flagsType)) == NULL) {
       printf("OpenCL returned a NULL context\n");
       return;
   }

   //Create and build the program using an OpenCL file.
   if ((program = createProgram(context, device, FILEPATH)) == NULL) {
       cleanup(context, queue, program, kernel, memObjects);
       printf("%s\n", "Failed to create a program.");
       return;
   }

   //Total number of work items
   global_size = 8;
   //Total number of work items per groups
   local_size = 4;
   //Total number of work groups
   numGroups = global_size / local_size;

   //dummy data array to put in buffer
   float data[ARRAY_SIZE];
   for (int i = 0; i < ARRAY_SIZE; ++i) {
       data[i] = i;
   }

   //Create the CL data buffer
   if ((memObjects[0] = createBuffer(context, CL_MEM_READ_ONLY |
        CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * sizeof(float), data)) == NULL) {
       cleanup(context, queue, program, kernel, memObjects);
       printf("%s\n", "Failed to create the input buffer.");
       return;
   }

   if ((memObjects[1] = createBuffer(context, CL_MEM_READ_WRITE |
        CL_MEM_COPY_HOST_PTR, ARRAY_SIZE * sizeof(float), data)) == NULL) {
       cleanup(context, queue, program, kernel, memObjects);
       printf("%s\n", "Failed to create the input buffer.");
       return;
   }

   //Create the command queue
   if ((queue = createCommandQueue(context, &device)) == NULL) {
       cleanup(context, queue, program, kernel, memObjects);
       printf("%s\n", "Failed to create a command queue.");
       return;
   }

   if ((kernel = createKernel(program, KERNEL_FUNCTION)) == NULL) {
       cleanup(context, queue, program, kernel, memObjects);
       printf("%s\n", "Failed to create a kernel.");
       return;
   }

   setKernelArgument(kernel, 0, sizeof(cl_mem), &memObjects[0]);
   setKernelArgument(kernel, 1, local_size * sizeof(float), NULL);
   setKernelArgument(kernel, 2, sizeof(cl_mem), &memObjects[1]);

   //Queue the kernel command to execute when available
   enqueueKernel(queue, kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
   //Queue the buffer to read the kernel's output to host memory when available
   enqueueReadBuffer(queue, memObjects[1], CL_TRUE, 0, sizeof(sum), sum, 0, NULL, NULL);

   for (int i = 0; i < sizeof(sum) / sizeof(sum[0]); i++) {
       printf("%f\n", sum[i]);
   }

   cleanup(context, queue, program, kernel, memObjects);

}
