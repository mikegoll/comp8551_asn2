#include "../Headers/openCLWrappers.h"

cl_context createContext(cl_mem_flags typeFlag) {
    cl_platform_id platformId;
    cl_uint numPlatforms;

    //Get the current platform
    cl_int errNum = clGetPlatformIDs(1, &platformId, &numPlatforms);

    if (errNum != CL_SUCCESS) {
        printf("%s%d", "Failed to find any OpenCL platforms, errNo: ", errNum);
        return NULL;
    }

    if (numPlatforms <= 0) {
      printf("%s\n", "Failed to find any OpenCL platforms.");
    }

    printf("%s: %d %s\n", "Found ", numPlatforms, " platforms.");

    char pname[1024];
    size_t retSize;

    errNum = clGetPlatformInfo(platformId, CL_PLATFORM_NAME, sizeof(pname), (void *)pname, &retSize);
    if (errNum != CL_SUCCESS) {
        printf("%s%d\n", "Could not get platform info, errNo: ", errNum);
        return NULL;
    }


    printf("%s<%s>\n", "Selected platform: ", pname);

    cl_context_properties contextProps[] = {CL_CONTEXT_PLATFORM, (cl_context_properties) platformId, 0};

    if (contextProps == NULL) {
        printf("Context properties were null\n");
        return NULL;
    }

    //Create the OpenCL context to use later
    //CL_DEVICE_TYPE_GPU can also be CL_DEVICE_TYPE_CPU or CL_DEVICE_TYPE_ALL
    cl_context context = clCreateContextFromType(contextProps, typeFlag, NULL, NULL, &errNum);
    if (errNum != CL_SUCCESS) {
        printf("%s%d\n", "Failed to create an OpenCL GPU or CPU context. Errno: ", errNum);
        return NULL;
    }

    return context;
}

//Gets the available Devices
//Creates a command queue using that device
cl_command_queue createCommandQueue(cl_context context, cl_device_id *device) {
    cl_int numDevices;
    size_t retSize;

    //Get the number of available devices
    cl_int errNum = clGetContextInfo(context, CL_CONTEXT_NUM_DEVICES, sizeof(numDevices), (void *)&numDevices, &retSize);

    if (errNum != CL_SUCCESS) {
        printf("No OpenCL Devices available\n");
        return NULL;
    }

    cl_device_id * deviceList;
    deviceList = (cl_device_id *)malloc(numDevices * sizeof(cl_device_id));

    if ((errNum = clGetContextInfo(context, CL_CONTEXT_DEVICES, numDevices * sizeof(cl_device_id), (void *)deviceList, &retSize)) != CL_SUCCESS) {
        printf("%s%d", "Could not get device list, error: ", errNum);
        return NULL;
    }

    cl_command_queue queue = clCreateCommandQueue(context, deviceList[0], 0, NULL);
    if (queue == NULL) {
        printf("%s\n", "Failed to create command queue for device.");
        free(deviceList);
        return NULL;
    }

    *device = deviceList[0];
    free(deviceList);

    return queue;
}


//Read the OpenCL program from the file
//
cl_program createProgram(cl_context context, cl_device_id device, const char * name) {
    cl_int errNum;
    cl_program program;
    size_t programSize, log_size;
    char * programBuffer, * program_log;
    FILE * programHandle;

    programHandle = fopen(name, "r");
    if (programHandle == NULL) {
        printf("%s\n", "Couldn't open the program file.");
        exit(1);
    }

    fseek(programHandle, 0, SEEK_END);
    programSize = ftell(programHandle);
    rewind(programHandle);
    programBuffer = (char *)malloc(programSize + 1);
    programBuffer[programSize] = '\0';
    fread(programBuffer, sizeof(char), programSize, programHandle);
    fclose(programHandle);


    program = clCreateProgramWithSource(context, 1, (const char *)&programBuffer, &programSize, &errNum);
    if (errNum != CL_SUCCESS) {
        printf("%s\n", "Could not create the program.");
        exit(1);
    }

    free(programBuffer);

    if (errNum  = clBuildProgram(program, 0, NULL, NULL, NULL, NULL) < 0) {
        /* Can use this to find the error cause. */
        /* Find size of log and print to std output */
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
            0, NULL, &log_size);
        program_log = (char*) malloc(log_size + 1);
        program_log[log_size] = '\0';
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
            log_size + 1, program_log, NULL);
        printf("%s\n", program_log);
        free(program_log);
        exit(1);
    }

    return program;
}

cl_mem createBuffer(cl_context context, cl_mem_flags flags, size_t dataSize, void * data) {
    cl_int errNum;
    cl_mem buffer;

    buffer = clCreateBuffer(context, flags, dataSize, data, &errNum);
    if (errNum != CL_SUCCESS) {
        printf("%s: %d", "Create Buffer Error:", errNum);
        return NULL;
    }

    return buffer;
}

cl_kernel createKernel(cl_program program, const char * functionName) {
    cl_int errNum;
    cl_kernel kernel;

    kernel = clCreateKernel(program, functionName, &errNum);
    if (errNum != CL_SUCCESS) {
        printf("%s: %d", "Create Kernel Error:", errNum);
        return NULL;
    }

    return kernel;
}

void setKernelArgument(cl_kernel kernel, cl_uint index, size_t size, const void * data) {
    cl_int errNum;

    if ((errNum = clSetKernelArg(kernel, index, size, data)) != CL_SUCCESS) {
        printf("%s: %d", "Kernel Argument Error:", errNum);
    }
}

void enqueueKernel(cl_command_queue queue, cl_kernel kernel, cl_uint workDimensions,
    const size_t * globalWorkOffset, const size_t * globalWorkSize, const size_t * localWorkSize,
    cl_uint eventsInWaitList, const cl_event * eventWaitList, cl_event * event) {

    cl_int errNum;

    if ((errNum = clEnqueueNDRangeKernel(queue, kernel, workDimensions, globalWorkOffset,
        globalWorkSize, localWorkSize, eventsInWaitList, eventWaitList, event)) != CL_SUCCESS) {

        printf("%s: %d", "Could not queue up kernel", errNum);
    }
}

void enqueueReadBuffer(cl_command_queue queue, cl_mem buffer, cl_bool blockingRead,
    size_t offsetToReadFrom, size_t sizeToRead, void * bufferToReadTo, cl_uint eventsInWaitList,
    const cl_event * eventWaitList, cl_event * event) {

    cl_int errNum;

    if ((errNum = clEnqueueReadBuffer(queue, buffer, blockingRead, offsetToReadFrom, sizeToRead, bufferToReadTo, eventsInWaitList, eventWaitList, event)) != CL_SUCCESS) {
        printf("%s: %d", "Could not queue up buffer to read.", errNum);
    }
}

void cleanup(cl_context context, cl_command_queue queue, cl_program program, cl_kernel kernel, cl_mem memObjects[3]) {
    //Release the memory objects one by one
    for (int i = 0; i < 3; ++i) {
        if (memObjects[i] != 0) {
            clReleaseMemObject(memObjects[i]);
        }
    }

    if (queue != 0) {
        clReleaseCommandQueue(queue);
    }

    if (kernel != 0) {
        clReleaseKernel(kernel);
    }

    if (program != 0) {
        clReleaseProgram(program);
    }

    if (context != 0) {
        clReleaseContext(context);
    }
}
