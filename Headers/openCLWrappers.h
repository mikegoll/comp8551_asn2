#ifndef OPENCL_WRAPPER
#define OPENCL_WRAPPER

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

/************************************************
* CreateContext
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Creates an OpenCL context, specifying which device.
************************************************/
cl_context createContext(cl_mem_flags);

/************************************************
* createCommandQueue
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Creates an OpenCL command queue.
************************************************/
cl_command_queue createCommandQueue(cl_context, cl_device_id*);

/************************************************
* createDevice
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Creates an OpenCL device.
************************************************/
cl_device_id createDevice();

/************************************************
* createProgram
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Creates an OpenCL program.
************************************************/
cl_program createProgram(cl_context, cl_device_id, const char *);

/************************************************
* createBuffer
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Creates an OpenCL buffer.
************************************************/
cl_mem createBuffer(cl_context, cl_mem_flags, size_t, void *);

/************************************************
* createKernel
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Creates an OpenCL kernel.
************************************************/
cl_kernel createKernel(cl_program, const char *);

/************************************************
* setKernelArgument
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Sets a kernel argument on a specific kernel.
************************************************/
void setKernelArgument(cl_kernel, cl_uint, size_t, const void *);

/************************************************
* enqueueKernel
*
* Created By: Michael Goll
* Created On: October 10, 2018
*
* Adds the kernel to the queue of execution.
************************************************/
void enqueueKernel(cl_command_queue, cl_kernel, cl_uint, const size_t *, const size_t *, const size_t *, cl_uint, const cl_event * , cl_event *);

/************************************************
* enqueueReadBuffer
*
* Created By: Michael Goll
* Created On: October 10, 2018
*
* Adds a data buffer to be read to when available.
************************************************/
void enqueueReadBuffer(cl_command_queue, cl_mem, cl_bool, size_t, size_t, void *, cl_uint, const cl_event *, cl_event *);

/************************************************
* cleanup
*
* Created By: Michael Goll
* Created On: October 3, 2018
*
* Performs a cleanup and frees resources.
************************************************/
void cleanup(cl_context, cl_command_queue, cl_program, cl_kernel, cl_mem[3]);

#endif
