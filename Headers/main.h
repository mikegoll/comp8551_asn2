#ifndef MAIN_H
#define MAIN_H

#define CL_SILENCE_DEPRECATION

#define FILEPATH "../OpenCL/pathfinding.cl"
#define KERNEL_FUNCTION "findPath"

#define ARRAY_SIZE 64

int main();
void setupOpenCL();

#endif
